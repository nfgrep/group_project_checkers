﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkers.Logic
{
    class Player
    {
        public string p1 { get; private set; }
        public string p2 { get; private set; }

        //Holds player info
        public Player(string name1, string name2)
        {
            p1 = name1;
            p2 = name2;
        }
    }
}
