﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Json;


namespace Checkers.Logic
{
    class JsonHandler
    {
        //The file path to be acted upon
        string fileName;

        /// <summary>
        /// A basic json handler that allows for generic use accross types
        /// </summary>
        /// <param name="filename"></param>
        public JsonHandler(string filename)
        {
            fileName = filename;
        }

        /// <summary>
        /// Saves a given object as json
        /// </summary>
        /// <param name="obj"></param>
        public void Store(Object obj)
        {
            try
            {
                DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(obj.GetType());
                using (FileStream fileStreamSave = new FileStream(fileName, FileMode.Create))
                {
                    jsonSerializer.WriteObject(fileStreamSave, obj);
                }
            }
            catch(NullReferenceException)
            {
                DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(string));
                using (FileStream fileStreamSave = new FileStream(fileName, FileMode.Create))
                {
                    jsonSerializer.WriteObject(fileStreamSave,"");
                }
            }
        }

        /// <summary>
        /// Loads an object based on the set filename and a type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public Object Load(Type type)
        {
            Object returnObj = null;
            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(type);
            using (FileStream fileStreamOpen = new FileStream(fileName,FileMode.Open))
            {
                returnObj = jsonSerializer.ReadObject(fileStreamOpen);
            }
            return returnObj;
        }
    }
}
