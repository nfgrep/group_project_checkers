﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;


namespace Checkers.Logic
{
    class BlackPiece : Piece
    {
        /// <summary>
        /// Represents a black checker piece
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="pieceImg"></param>
        public BlackPiece(int col, int row, Image pieceImg): base(col,row,pieceImg)
        {

        }

        /// <summary>
        /// Returns whether or not this peice can move to the tapped tile
        /// </summary>
        /// <param name="tappedTile"></param>
        /// <returns></returns>
        public override bool CanMoveTo(Tile tappedTile)
        {
            bool canMove = false;
            if (this.Column > 0 || this.Column < 7)
            {

                if (tappedTile.Column == (this.Column + 1) || tappedTile.Column == (this.Column - 1))
                {
                    if (isKing)
                    {
                        if (tappedTile.Row == this.Row + 1 || tappedTile.Row == this.Row - 1)
                        {
                            canMove = true;
                        }
                    }
                    else
                    {
                        if (tappedTile.Row == this.Row + 1)
                        {
                            canMove = true;
                        }
                    }
                }
            }
            else if (Column == 0)
            {
                if (tappedTile.Column == (this.Column + 1))
                {
                    canMove = true;
                }
            }
            else
            {
                if (tappedTile.Column == (this.Column -1))
                {
                    canMove = true;
                }
            }
            return canMove;
        }
    }
}
