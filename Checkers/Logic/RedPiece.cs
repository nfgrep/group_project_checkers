﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;


namespace Checkers.Logic
{
    class RedPiece : Piece
    {
        /// <summary>
        /// Represents a red checker piece
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="pieceImg"></param>
        public RedPiece(int col, int row, Image pieceImg) : base(col,row,pieceImg)
        {
        }

        /// <summary>
        /// Checks if piece can move to tapped tile
        /// </summary>
        /// <param name="tappedTile"></param>
        /// <returns></returns>
        public override bool CanMoveTo(Tile tappedTile)
        {
            bool canMove = false;
            //Checks if on edge
            if (this.Column > 0 && this.Column < 7)
            {
                //Checks if within range
                if (tappedTile.Column == (this.Column + 1) || tappedTile.Column == (this.Column - 1))
                {
                    if (isKing)
                    {
                        if (tappedTile.Row == this.Row - 1 || tappedTile.Row == this.Row + 1)
                        {
                            canMove = true;
                        }
                    }
                    else
                    {
                        if (tappedTile.Row == this.Row - 1)
                        {
                            canMove = true;
                        }
                    }
                }
            }
            //If on left edge
            else if (Column == 0)
            {
                if (tappedTile.Column == this.Column + 1)
                {
                    canMove = true;
                }
            }
            //If on right edge
            else
            {
                if (tappedTile.Column == (this.Column - 1))
                {
                    canMove = true;
                }
            }
            return canMove;
        }
    }
}
