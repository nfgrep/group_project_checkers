﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace Checkers.Logic
{
    public class Tile
    {
        public int Row {get; private set;}
        public int Column { get; private set; }
        public BitmapImage TileImage { get; private set; }
        /// <summary>
        /// Represents a tile on the board
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <param name="tileImg"></param>
        public Tile(int col, int row, BitmapImage tileImg)
        {
            TileImage = tileImg;
            Column = col;
            Row = row;
        }
    }
}
