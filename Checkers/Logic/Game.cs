﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Storage;

namespace Checkers.Logic
{
    class Game
    {
        //Images for king pieces   
        BitmapImage redKingPieceImgSrc;
        BitmapImage blackKingPieceImgSrc;

        //References to the currently selected piece
        private Image selectedPieceImg;
        private Piece selectedPiece;

        //2D collection of pieces as an external reference to theyre position
        public Piece[,] posPieces;
        //Collection of pieces and piece images
        public Dictionary<Image, Piece> imgPieces;

        //Score and player values
        public int turn;
        int p1Score;
        int p2Score;
        public string p1Name;
        public string p2Name;
        public bool gameWon;

        //Represents whether a piece is selected or not
        bool selectingLanding;

        /// <summary>
        /// Handles main game logic
        /// </summary>
        public Game()
        {
            selectingLanding = false;
            gameWon = false;
            turn = 0;
            p1Score = 0;
            p2Score = 0;
            redKingPieceImgSrc = new BitmapImage(new Uri("ms-appx:///Assets/red_king_piece.PNG"));
            blackKingPieceImgSrc = new BitmapImage(new Uri("ms-appx:///Assets/black_king_piece.PNG"));
            imgPieces = new Dictionary<Image, Piece>();
            posPieces = new Piece[8, 8];
            
        }
        
        /// <summary>
        /// Logic checks when a tile is tapped
        /// </summary>
        /// <param name="senderTile"></param>
        public void OnTileTap(Tile senderTile)
        {
            if (selectingLanding)
            {
                if (JumpPiece(selectedPiece, senderTile) || selectedPiece.CanMoveTo(senderTile))
                {
                    selectedPiece.MovePieceTo(senderTile);
                    if (senderTile.Row > 6 || senderTile.Row < 1)
                    {
                        if (selectedPiece is BlackPiece)
                        {
                            selectedPiece.PieceImage.SetValue(Image.SourceProperty, blackKingPieceImgSrc);
                        }
                        else
                        {
                            selectedPiece.PieceImage.SetValue(Image.SourceProperty, redKingPieceImgSrc);
                        }

                        selectedPiece.isKing = true;

                        System.Diagnostics.Debug.WriteLine("KINGARU!!~~");

                    }
                    posPieces[selectedPiece.Column, selectedPiece.Row] = null;
                    posPieces[senderTile.Column, senderTile.Row] = selectedPiece;
                    selectingLanding = false;
                    turn++;
                    System.Diagnostics.Debug.WriteLine("p1SCORE" + p1Score);
                    System.Diagnostics.Debug.WriteLine("p2SCORE" + p2Score);

                    if (p1Score > 11 || p2Score > 11)
                    {
                        JsonHandler json = new JsonHandler(ApplicationData.Current.LocalFolder.Path + "\\" + "records.json");
                        GameRecord gr = new GameRecord(p1Score,p2Score,p1Name,p2Name,turn);
                        json.Store(null);
                        json.Store(gr);
                        gameWon = true;
                    }
                }

                

            }
            else
            {
                System.Diagnostics.Debug.WriteLine("Please Select a piece to move");
            }
        }

        /// <summary>
        /// Logic checks for when a piece is tapped
        /// </summary>
        /// <param name="senderImg"></param>
        public void OnPieceTap(Image senderImg)
        {
            //Assigns the spcific value of Red or Black piece from stored Piece type
            if (imgPieces[senderImg] is BlackPiece)
            {
                imgPieces[senderImg] = (BlackPiece)imgPieces[senderImg];
            }
            else
            {
                imgPieces[senderImg] = (RedPiece)imgPieces[senderImg];
            }
            
            if (selectingLanding)
            {
                if (selectedPiece.GetType() == imgPieces[senderImg].GetType())
                {
                    selectedPiece = imgPieces[senderImg];
                    selectingLanding = true;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("ThisIsNotYourPiece");
                }
            }
            else
            {
                if (turn % 2 == 0 && imgPieces[senderImg].GetType() == typeof(BlackPiece))
                {
                    selectedPiece = imgPieces[senderImg];
                    selectingLanding = true;
                }
                else if (turn % 2 != 0 && imgPieces[senderImg].GetType() == typeof(RedPiece))
                {
                    selectedPiece = imgPieces[senderImg];
                    selectingLanding = true;
                }
                else
                {
                    System.Diagnostics.Debug.WriteLine("NOT YER PEICE");
                }
            }
        }

        /// <summary>
        /// Sets the selected piece
        /// </summary>
        /// <param name="tappedPiece"></param>
        /// <param name="tappedPieceImg"></param>
        private void SelectPieceImage(Piece tappedPiece, Image tappedPieceImg)
        {
            selectedPiece = tappedPiece;

            selectedPieceImg = tappedPieceImg;

            selectingLanding = true;
        }

        /// <summary>
        /// Checks for conditions to jump
        /// </summary>
        /// <param name="piece"></param>
        /// <param name="tile"></param>
        /// <returns>result</returns>
        private bool JumpPiece(Piece piece, Tile tile)
        {
            bool jump = false;
            int xComponent = tile.Column - piece.Column;
            int yComponent = tile.Row - piece.Row;
            if (Math.Abs(xComponent) == 2 && Math.Abs(yComponent) == 2)
            {
                //If tapped tile is above piece
                if (Math.Sign(yComponent) == -1)
                {
                    if (piece is RedPiece || piece.isKing == true)
                    {
                        //If tapped tile is left of piece
                        if (Math.Sign(xComponent) == -1)
                        {
                            if (posPieces[tile.Column + 1, tile.Row + 1].GetType() != piece.GetType())
                            {
                                Image rmThisImg = posPieces[tile.Column + 1, tile.Row + 1].PieceImage;
                                Grid mainGrid = (Grid)rmThisImg.Parent;
                                imgPieces.Remove(rmThisImg);
                                mainGrid.Children.Remove(rmThisImg);
                                posPieces[tile.Column + 1, tile.Row + 1] = null;
                                System.Diagnostics.Debug.WriteLine("REMOVEPIECE");
                                jump = true;
                            }

                        }
                        //If tapped tile is right of piece
                        else
                        {
                                if (posPieces[tile.Column - 1, tile.Row + 1].GetType() != piece.GetType())
                                {
                                    Image rmThisImg = posPieces[tile.Column - 1, tile.Row + 1].PieceImage;
                                    Grid mainGrid = (Grid)rmThisImg.Parent;
                                    imgPieces.Remove(rmThisImg);
                                    mainGrid.Children.Remove(rmThisImg);
                                    posPieces[tile.Column - 1, tile.Row + 1] = null;
                                    System.Diagnostics.Debug.WriteLine("YALL KILLED A MFN PIECE FR RN YO!!");
                                    jump = true;
                                }
                            
                        }
                    }
                }
                //If tapped tile is below piece
                else
                {
                    if (piece is BlackPiece || piece.isKing == true) {
                        if (Math.Sign(xComponent) == -1)
                        {
                              if (posPieces[tile.Column + 1, tile.Row - 1].GetType() != piece.GetType())
                                {
                                    Image rmThisImg = posPieces[tile.Column + 1, tile.Row - 1].PieceImage;
                                    Grid mainGrid = (Grid)rmThisImg.Parent;
                                    imgPieces.Remove(rmThisImg);
                                    mainGrid.Children.Remove(rmThisImg);
                                    posPieces[tile.Column + 1, tile.Row - 1] = null;
                                    jump = true;
                                }
                            
                        }
                        else
                        {
                            if (posPieces[tile.Column - 1, tile.Row - 1].GetType() != piece.GetType())
                            {
                                Image rmThisImg = posPieces[tile.Column - 1, tile.Row - 1].PieceImage;
                                Grid mainGrid = (Grid)rmThisImg.Parent;

                                imgPieces.Remove(rmThisImg);
                                mainGrid.Children.Remove(rmThisImg);
                                posPieces[tile.Column - 1, tile.Row - 1] = null;
                                jump = true;
                            }
                        }
                    }
                }
            }
            if (jump)
            {
                if (piece is BlackPiece)
                {
                    p1Score++;
                }
                else
                {
                    p2Score++;
                }
            }
            return jump;
        }
    }
}
