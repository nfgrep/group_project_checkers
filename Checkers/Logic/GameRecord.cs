﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Checkers.Logic
{
    [DataContract]
    class GameRecord
    {
        [DataMember]
        public string p1Name;
        [DataMember]
        public string p2Name;
        [DataMember]
        public int p1Score;
        [DataMember]
        public int p2Score;
        [DataMember]
        public int turns;
        /// <summary>
        /// Holds values for the record board
        /// </summary>
        /// <param name="p1score"></param>
        /// <param name="p2score"></param>
        /// <param name="p1name"></param>
        /// <param name="p2name"></param>
        /// <param name="turns"></param>
        public GameRecord(int p1score, int p2score, string p1name ,string p2name ,int turns)
        {
            p1Name = p1name;
            p2Name = p2name;
            p1Score = p1score;
            p2Score = p2score;
            this.turns = turns;

        }
    }
}
