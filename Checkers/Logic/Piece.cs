﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;
using System.ComponentModel;
using Windows.UI.Xaml.Controls;

namespace Checkers.Logic
{
    abstract class Piece 
    {
        private int row;
        public int Row
        {
            get
            { return row; }
            //Updates the grid position on set
            set
            {
                PieceImage.SetValue(Grid.RowProperty, value);
                
                row = value;
            }
        }

        private int column;
        public int Column
        {
            get { return column; }
            set
            {
                //Updates the grid row positoin on set
                PieceImage.SetValue(Grid.ColumnProperty, value);
                column = value;
            }
        }

        //Reference to the image of the piece in XAML UI
        public Image PieceImage{ get; set; }

        //Holds king value, moveset adjustments made is this true
        public bool isKing { get; set; }

        /// <summary>
        /// Base class for all pieces
        /// </summary>
        /// <param name="col"></param>
        /// <param name="rw"></param>
        /// <param name="img"></param>
        public Piece(int col, int rw, Image img)
        {
            column = col;
            row = rw;
            PieceImage = img;
        }

        /// <summary>
        /// Changes values according to move
        /// </summary>
        /// <param name="senderTile"></param>
        public void MovePieceTo(Tile senderTile)
        {
            this.Column = senderTile.Column;
            this.Row = senderTile.Row;
        }

        /// <summary>
        /// Abstract method for all pieces to innherit
        /// </summary>
        /// <param name="tappedTile"></param>
        /// <returns></returns>
        public abstract bool CanMoveTo(Tile tappedTile);

        


       
    }
}
