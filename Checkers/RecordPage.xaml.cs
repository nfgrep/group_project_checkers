﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Checkers.Logic;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Checkers
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class RecordPage : Page
    {
        GameRecord gr;

        /// <summary>
        /// Displays the record and scores of last played game
        /// </summary>
        public RecordPage()
        {
            this.InitializeComponent();

        }

        /// <summary>
        /// Sets XAML elements to corresponding values in GameRecord object
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            JsonHandler json = new JsonHandler(ApplicationData.Current.LocalFolder.Path + "\\" + "records.json");
            try
            {
                gr = (GameRecord)json.Load(typeof(GameRecord));
                txtP1Name.Text = gr.p1Name + " Score:";
                txtP2Name.Text = gr.p2Name + " Score:";
                txtP1Score.Text = gr.p1Score.ToString();
                txtP2Score.Text = gr.p2Score.ToString();
                txtTurns.Text = gr.turns.ToString();
                if (gr.p1Score > gr.p2Score)
                {
                    txtWinner.Text = gr.p1Name;
                }
                else
                {
                    txtWinner.Text = gr.p2Name;
                }
                
            }
            catch (Exception)
            {
                System.Diagnostics.Debug.WriteLine("No game data saved yet, someone has to win first!");
            }
            

        }
    }
}
