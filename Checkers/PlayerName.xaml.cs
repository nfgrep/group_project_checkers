﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Checkers.Logic;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Checkers
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PlayerName : Page
    {
        Player players;
        /// <summary>
        /// Borders main play-space with names
        /// </summary>
        public PlayerName()
        {
            this.InitializeComponent();
            
        }

        /// <summary>
        /// Sets the XAML elements to the player names
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            players = (Player)e.Parameter;
            player1.Text = players.p1;
            player2.Text = players.p2;

            frmGame.Navigate(typeof(MainPage), players);
        }

    }
}
