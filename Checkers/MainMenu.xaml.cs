﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Checkers
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainMenu : Page
    {

        //used to store the game records
        Windows.Storage.ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
        Windows.Storage.StorageFolder localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;

        public MainMenu()
        {
            this.InitializeComponent();
        }

        private void SvLBoard_Tapped(object sender, TappedRoutedEventArgs e)
        {
            FrmCheckers.Navigate(typeof(RecordPage));
        }

        private void SvPlay_Tapped(object sender, TappedRoutedEventArgs e)
        {
            FrmCheckers.Navigate(typeof(NewPlayers));
        }

        private void SvExit_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Application.Current.Exit();
        }

        private void NavButton_Click(object sender, RoutedEventArgs e)
        {
            if (!svMainWindow.IsPaneOpen)
                svMainWindow.IsPaneOpen = true;
            else
                svMainWindow.IsPaneOpen = false;
        }
    }
}
