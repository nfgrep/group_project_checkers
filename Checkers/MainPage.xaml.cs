﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Checkers.Logic;
using Windows.UI.Xaml.Media.Imaging;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Checkers
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        //Images of tiles and pieces to be instantiated
        BitmapImage whiteTileImgSrc;
        BitmapImage blackTileImgSrc;
        BitmapImage redPieceImgSrc;
        BitmapImage blackPieceImgSrc;
        
        Game game;
        
        //A collection of tiles and their images
        Dictionary<Image,Tile> imgTiles;

        /// <summary>
        /// The main page for which the checkers gameplay takes place
        /// </summary>
        public MainPage()
        {
            imgTiles = new Dictionary<Image, Tile>();
            this.InitializeComponent();
            whiteTileImgSrc = new BitmapImage(new Uri("ms-appx:///Assets/white_tile.jpg"));
            blackTileImgSrc = new BitmapImage(new Uri("ms-appx:///Assets/black_tile.jpg"));
            redPieceImgSrc = new BitmapImage(new Uri("ms-appx:///Assets/red_piece.PNG"));
            blackPieceImgSrc = new BitmapImage(new Uri("ms-appx:///Assets/black_piece.PNG"));
        }

        /// <summary>
        /// Passes player variables onto Game class
        /// </summary>
        /// <param name="e"></param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            game = new Game();
            Player players = (Player)e.Parameter;
            game.p1Name = players.p1;
            game.p2Name = players.p2;
            System.Diagnostics.Debug.WriteLine("_--_MAINPAGE--NAMES----" + players.p1 + players.p2);
            
        }

        /// <summary>
        /// Calls the PieceTap funciton in Game, sending the image of corresponding piece
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Piece_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Image tappedImg = sender as Image;
            game.OnPieceTap(tappedImg);
        }

        /// <summary>
        /// Same as above, but with tiles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Tile_Tapped(object sender, TappedRoutedEventArgs e)
        {
            game.OnTileTap(imgTiles[(Image)sender]);
            if (game.gameWon)
            {
                Frame.Navigate(typeof(RecordPage));
            }
            
            
        }

        ///Instantiates tiles on page_loaded
        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("PageLoad");
            SetTiles();
            SetPieces();
        }

        /// <summary>
        /// Instantiates tiles based on position
        /// </summary>
        private void SetTiles()
        {
            for (int row = 0; row < 8; row++)
            {
                for (int col = 0; col < 8; col++)
                {
                    if ((float)col % 2 == 0.0)
                    {
                        if ((float)row % 2 == 0.0)
                        {
                            InstantiateTile(col, row, whiteTileImgSrc, false);
                        }
                        else
                        {
                            InstantiateTile(col, row, blackTileImgSrc, true);
                        }
                    }
                    else
                    {
                        if ((float)row % 2 != 0.0)
                        {
                            InstantiateTile(col, row, whiteTileImgSrc, false);
                        }
                        else
                        {
                            InstantiateTile(col, row, blackTileImgSrc, true);
                        }
                    }
                }
            }
        }

        private void InstantiateTile(int col, int row, BitmapImage bMap, bool isTapp)
        {
            Tile thisTile = new Tile(col, row, bMap);

            Binding bindBitmap = new Binding(); //Binds the bitmap image stored in Piece, to the image element in XAML
            bindBitmap.Mode = BindingMode.OneTime;
            bindBitmap.Source = thisTile.TileImage;
            Binding bindCol = new Binding();    //Binds the column value from Piece obj to the grid column in XAML
            bindCol.Mode = BindingMode.OneTime;
            bindCol.Source = thisTile.Column;
            Binding bindRow = new Binding();    //Binds the row value from Piece obj to the grid row in XAML
            bindRow.Mode = BindingMode.OneTime;
            bindRow.Source = thisTile.Row;

            Image thisTileImage = new Image(); //Instantiates new image
            thisTileImage.SetBinding(Image.SourceProperty, bindBitmap);
            thisTileImage.SetBinding(Grid.ColumnProperty, bindCol);
            thisTileImage.SetBinding(Grid.RowProperty, bindRow);

            mainGrid.Children.Add(thisTileImage);   //Adds a new image to the grid

            if (isTapp)
            {
                thisTileImage.IsTapEnabled = true;  //Adds tap functionality
                thisTileImage.Tapped += Tile_Tapped;    //Adds the tile_tapped property to the newly added image
            }
            imgTiles.Add(thisTileImage, thisTile);
        }

        /// <summary>
        /// Instantiates pieces based on position
        /// </summary>
        private void SetPieces()
        {
            int row;
            int col;

            for (col = 0; col < 8; col++)
            {
                for (row = 0; row < 3; row++)
                {
                    if ((float)(col+row) %2 != 0.0)
                    {
                        InstantiatePiece(col, row, blackPieceImgSrc, false );
                    }
                }

                for (row = 5; row < 8; row++)
                {
                    if ((float)(col + row) % 2 != 0.0)
                    {
                        InstantiatePiece(col, row, redPieceImgSrc, true);
                    }
                }
            }
        }

        private void InstantiatePiece(int col, int row, BitmapImage bMap, bool isRedPiece)
        {
            Image thisPieceImage = new Image(); //Instantiates new image
            thisPieceImage.SetValue(Image.SourceProperty, bMap);

            if (isRedPiece)
            {
                RedPiece thisPiece = new RedPiece(col, row, thisPieceImage)
                {
                    Row = row,
                    Column = col
                };
                game.imgPieces.Add(thisPieceImage, thisPiece);
                game.posPieces[col, row] = thisPiece;
            }
            else
            {
                BlackPiece thisPiece = new BlackPiece(col, row, thisPieceImage)
                {
                    Row = row,
                    Column = col
                };
                game.imgPieces.Add(thisPieceImage, thisPiece);
                game.posPieces[col, row] = thisPiece;
            }
            
            thisPieceImage.SetValue(Grid.RowProperty, row);
            thisPieceImage.SetValue(Grid.ColumnProperty, col);

            mainGrid.Children.Add(thisPieceImage);   //Adds a new image to the grid

            thisPieceImage.IsTapEnabled = true;  //Adds tap functionality
            thisPieceImage.Tapped += Piece_Tapped;    //Adds the tile_tapped property to the newly added image
        }

        /// <summary>
        /// Removes a piece image
        /// </summary>
        /// <param name="img"></param>
        public void RemovePieceImg(Image img)
        {
            mainGrid.Children.Remove(img);
        }

        /// <summary>
        /// Responds to SkipTurn button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            game.turn++;
        }
    }
}
