﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Checkers.Logic;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Checkers
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class NewPlayers : Page
    {
        public NewPlayers()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Checks for names and sets them in Player object
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnPlay_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(player1.Text) && !string.IsNullOrEmpty(player2.Text))
            {
                Player players = new Player(player1.Text, player2.Text);
                Frame.Navigate(typeof(PlayerName), players);
            }
            else
            {
                txtError.Text = "Please input both names properly";
            }
            
        }
    }
}
